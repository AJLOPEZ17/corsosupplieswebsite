<?php

namespace Tests\Feature;

use App\Models\Innovation\PresentationCardInnovation\PresentationCardInnovation;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class PresentationCardInnovationTest extends TestCase
{
 use RefreshDatabase, WithFaker;

 protected $presentationCardInnovation;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user                       = User::first();
  $this->presentationCardInnovation = PresentationCardInnovation::first();
 }

 /** @test */
 public function can_read_presentation_card_innovation()
 {
  $this->actingAs($this->user)->get('/api/auth/innovation/presentationCardsInnovation')
   ->assertStatus(200);
 }

 /** @test */
 public function can_update_presentation_card_innovation()
 {
  $this->put('/api/auth/innovation/presentationCardsInnovation/' . $this->presentationCardInnovation->id, [
   'title_one'       => $this->faker->words(3, true),
   'description_one' => $this->faker->text,
   'title_two'       => $this->faker->words(3, true),
   'description_two' => $this->faker->text,
  ])->assertStatus(200);
 }
}
