<?php

namespace Tests\Feature;

use App\Models\Home\Slider\Slider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class SliderTest extends TestCase
{
 use RefreshDatabase, WithFaker;
 protected $slider;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->slider = Slider::first();
 }
 /** @test */
 public function can_create_slider_image()
 {
  $this->post('/api/auth/home/slider', [
   'title'      => $this->faker->words(3, true),
   'image'      => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name' => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_create_slider_url()
 {
  $this->post('/api/auth/home/slider', [
   'url' => $this->faker->url,
  ])->assertStatus(200);
 }
 /** @test */
 public function can_update_slider_image()
 {
  $response = $this->put('/api/auth/home/slider/' . $this->slider->id, [
   'title'      => $this->faker->words(3, true),
   'image'      => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name' => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_update_slider_url()
 {
  $this->put('/api/auth/home/slider/' . $this->slider->id, [
   'url' => $this->faker->url,
  ])->assertStatus(200);
 }
 /** @test */
 public function can_read_slider()
 {
  $this->get('/api/auth/home/slider', [
  ])->assertStatus(200);

 }
 /** @test */
 public function can_read_a_slider()
 {
  $this->get('/api/auth/home/slider/' . $this->slider->id, [
  ])->assertStatus(200);
 }
 /** @test */
 public function can_delete_slider()
 {
  $this->delete('/api/auth/home/slider/2', [
  ])->assertStatus(200);
 }

}
