<?php

namespace Tests\Feature;

use App\Models\General\Banner\Banner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class BannerTest extends TestCase
{
 use RefreshDatabase, WithFaker;
 protected $banner;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->banner = Banner::first();
 }

 /** @test */
 public function can_update_banner_images()
 {
  $this->put('/api/auth/general/banner/' . $this->banner->id, [
   'title'      => $this->faker->words(3, true),
   'image'      => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name' => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_read_customer_images()
 {
  $this->get('/api/auth/general/banner', [
  ])->assertStatus(200);

 }
}
