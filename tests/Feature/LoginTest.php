<?php

namespace Tests\Feature;

use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class LoginTest extends TestCase
{
 use RefreshDatabase;
 protected $user;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user = User::first();
 }

 /** @test */
 public function can_login_with_credentials()
 {
  $this->post('/api/auth/login', [
   'email'    => $this->user->email,
   'password' => 'superadmin',
  ])->assertStatus(200);

 }
 /** @test */
 public function can_not_login_without_credentials()
 {
  $this->post('/api/auth/login', [
   'email'    => 'user@example.com',
   'password' => 'password',
  ])->assertStatus(401);

 }
}
