<?php

namespace Tests\Feature;

use App\Models\Home\Service\Service;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ServicesTest extends TestCase
{
 use RefreshDatabase, WithFaker;

 protected $services;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user     = User::first();
  $this->services = Service::first();
 }

 /** @test */
 public function can_read_services()
 {
  $this->actingAs($this->user)->get('/api/auth/home/services')
   ->assertStatus(200);
 }

 /** @test */
 public function can_update_services()
 {
  $this->put('/api/auth/home/services/' . $this->services->id, [
   'title'       => $this->faker->words(3, true),
   'description' => $this->faker->text,
  ])->assertStatus(200);
 }
}
