<?php

namespace Tests\Feature;

use App\Models\Products\ProductCategory\ProductCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ProductCategoryTest extends TestCase
{
 use RefreshDatabase, WithFaker;
 protected $productCategory;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->productCategory = ProductCategory::first();
 }
 /** @test */
 public function can_create_product_categories()
 {
  $this->post('/api/auth/product/productCategory', [
   'name'       => $this->faker->word,
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_update_product_categories()
 {
  $this->put('/api/auth/product/productCategory/' . $this->productCategory->id, [
   'name' => $this->faker->word,
  ])->assertStatus(200);
 }
 /** @test */
 public function can_read_product_categories()
 {
  $response = $this->get('/api/auth/product/productCategory', [
  ])->assertStatus(200);

 }
 /** @test */
 public function can_read_a_product()
 {
  $this->get('/api/auth/product/productCategory/' . $this->productCategory->id, [
  ])->assertStatus(200);

 }
}
