<?php

namespace Tests\Feature;

use App\Models\AboutUs\PresentationCards\PresentationCards;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class PresentationCardsTest extends TestCase
{
 use RefreshDatabase, WithFaker;

 protected $presentationCard;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user             = User::first();
  $this->presentationCard = PresentationCards::first();
 }

 /** @test */
 public function can_read_presentation_cards()
 {
  $this->actingAs($this->user)->get('/api/auth/aboutUs/presentationCards')
   ->assertStatus(200);
 }

 /** @test */
 public function can_update_presentation_cards()
 {
  $this->put('/api/auth/aboutUs/presentationCards/' . $this->presentationCard->id, [
   'title'       => $this->faker->words(3, true),
   'description' => $this->faker->text,
  ])->assertStatus(200);
 }
}
