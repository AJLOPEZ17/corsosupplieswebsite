<?php

namespace Tests\Feature;

use App\Models\Home\InformationBanner\InformationBanner;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class InformationBannerTest extends TestCase
{
 use RefreshDatabase, WithFaker;

 protected $user, $informationBanner;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user              = User::first();
  $this->informationBanner = InformationBanner::first();
 }

 /** @test */
 public function can_read_information_banner_with_credentials()
 {
  $this->actingAs($this->user)->get('/api/auth/home/informationBanner')
   ->assertStatus(200);
 }

 /** @test */
 public function can_update_information_banner_with_credentials()
 {
  $response = $this->put('/api/auth/home/informationBanner/' . $this->informationBanner->id, [
   'title'       => $this->faker->words(3, true),
   'description' => $this->faker->text(200),
  ])->assertStatus(200);
 }
}
