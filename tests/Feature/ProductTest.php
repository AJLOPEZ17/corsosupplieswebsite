<?php

namespace Tests\Feature;

use App\Models\Products\Product\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ProductTest extends TestCase
{
 use RefreshDatabase, WithFaker;
 protected $product;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->product = Product::first();
 }
 /** @test */
 public function can_create_customer_images()
 {
  $this->post('/api/auth/product', [
   'title'               => $this->faker->words(3, true),
   'description'         => $this->faker->text(),
   'ingredients'         => $this->faker->text(),
   'top'                 => $this->faker->numberBetween(0, 1),
   'product_category_id' => $this->faker->numberBetween(1, 4),
   'image'               => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name'          => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_update_customer_images()
 {
  $this->put('/api/auth/product/' . $this->product->id, [
   'title'               => $this->faker->words(3, true),
   'description'         => $this->faker->text(),
   'ingredients'         => $this->faker->text(),
   'top'                 => $this->faker->numberBetween(0, 1),
   'product_category_id' => $this->faker->numberBetween(1, 4),
   'image'               => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name'          => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_read_customer_images()
 {
  $this->get('/api/auth/product', [
  ])->assertStatus(200);

 }
 /** @test */
 public function can_read_a_customer_image()
 {
  $this->get('/api/auth/product/' . $this->product->id, [
  ])->assertStatus(200);
 }
 /** @test */
 public function can_delete_customer_images()
 {
  $this->delete('/api/auth/product/2', [
  ])->assertStatus(200);
 }
}
