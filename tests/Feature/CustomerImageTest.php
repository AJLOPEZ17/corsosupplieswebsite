<?php

namespace Tests\Feature;

use App\Models\Home\CustomerImage\CustomerImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class CustomerImageTest extends TestCase
{
 use RefreshDatabase, WithFaker;
 protected $customerImage;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->customerImage = CustomerImage::first();
 }
 /** @test */
 public function can_create_customer_images()
 {
  $response = $this->post('/api/auth/home/customerImage', [
   'name'       => $this->faker->words(3, true),
   'image'      => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name' => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_update_customer_images()
 {
  $this->put('/api/auth/home/customerImage/' . $this->customerImage->id, [
   'name'       => $this->faker->words(3, true),
   'image'      => UploadedFile::fake()->image('file.png', 600, 600),
   'image_name' => $this->faker->words(3, true),
  ])->assertStatus(200);
 }
 /** @test */
 public function can_read_customer_images()
 {
  $this->get('/api/auth/home/customerImage', [
  ])->assertStatus(200);

 }
 /** @test */
 public function can_read_a_customer_image()
 {
  $this->get('/api/auth/home/customerImage/' . $this->customerImage->id, [
  ])->assertStatus(200);
 }
 /** @test */
 public function can_delete_customer_images()
 {
  $this->delete('/api/auth/home/customerImage/2', [
  ])->assertStatus(200);
 }

}
