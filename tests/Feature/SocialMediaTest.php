<?php

namespace Tests\Feature;

use App\Models\General\SocialMedia\SocialMedia;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class SocialMediaTest extends TestCase
{
 use RefreshDatabase, WithFaker;

 protected $socialMedia;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user        = User::first();
  $this->socialMedia = SocialMedia::first();
 }

 /** @test */
 public function can_read_social_media()
 {
  $this->actingAs($this->user)->get('/api/auth/general/socialMedia')
   ->assertStatus(200);
 }

 /** @test */
 public function can_update_social_media()
 {
  $this->put('/api/auth/general/socialMedia/' . $this->socialMedia->id, [
   'longitude' => $this->faker->longitude(),
   'latitude'  => $this->faker->latitude(),
   'facebook'  => $this->faker->url,
   'twitter'   => $this->faker->url,
   'instagram' => $this->faker->url,
   'youtube'   => $this->faker->url,
   'whatsapp'  => $this->faker->url,
  ])->assertStatus(200);
 }
}
