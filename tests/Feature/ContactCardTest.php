<?php

namespace Tests\Feature;

use App\Models\Contact\ContactCard\ContactCard;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ContactCardTest extends TestCase
{
 use RefreshDatabase, WithFaker;

 protected $contactCard;
 public function setUp(): void
 {
  parent::setUp();
  Artisan::call('migrate');
  Artisan::call('db:seed');
  $this->user        = User::first();
  $this->contactCard = ContactCard::first();
 }

 /** @test */
 public function can_read_contact_card()
 {
  $this->actingAs($this->user)->get('/api/auth/contact/contactCard')
   ->assertStatus(200);
 }

 /** @test */
 public function can_update_contact_card()
 {
  $response = $this->put('/api/auth/contact/contactCard/' . $this->contactCard->id, [
   'address'      => $this->faker->address,
   'phone_number' => '3313118745',
   'email'        => $this->faker->email,
  ])->assertStatus(200);
 }
}
