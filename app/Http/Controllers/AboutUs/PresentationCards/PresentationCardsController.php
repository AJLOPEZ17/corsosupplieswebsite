<?php

namespace App\Http\Controllers\AboutUs\PresentationCards;

use App\Http\Controllers\Controller;
use App\Models\AboutUs\PresentationCards\PresentationCards;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PresentationCardsController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $presentationCards = PresentationCards::paginate(3);
  return $presentationCards;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\AboutUs\PresentationCards\PresentationCards  $presentationCards
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $presentationCard)
 {
  $data = PresentationCards::find($presentationCard);

  $validator = Validator::make($request->all(), [
   'title'       => 'required|max:50',
   'description' => 'required',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->title       = $request->title;
  $data->description = $request->description;

  $data->save();

  return response()->json($data);
 }

}
