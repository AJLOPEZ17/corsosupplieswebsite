<?php

namespace App\Http\Controllers\Contact\ContactCard;

use App\Http\Controllers\Controller;
use App\Models\Contact\ContactCard\ContactCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactCardController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $contactCard = ContactCard::first();
  return $contactCard;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Contact\ContactCard\ContactCard  $contactCard
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $contactCard)
 {
  $data = ContactCard::find($contactCard);

  $validator = Validator::make($request->all(), [
   'address'      => 'required|max:255',
   'phone_number' => 'required|digits:10',
   'email'        => 'required|email',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->address      = $request->address;
  $data->phone_number = $request->phone_number;
  $data->email        = $request->email;

  $data->save();

  return response()->json($data);
 }

}
