<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index(Request $request)
 {
  $users = User::paginate(1000);
  return $users;
 }
 /**
  * Display the specified resource.
  *
  * @param  \App\Models\User\User  $user
  * @return \App\Models\User\User
  */
 public function show(User $user)
 {
  return $user;
 }
/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \App\Models\User\User  $user
 * @return \Illuminate\Http\Response
 */
 public function update(Request $request, $user)
 {

  $data = User::find($user);

  $validator = Validator::make($request->all(), [
   'name'     => 'string|max:255',
   'email'    => 'string|email|max:255',
   'password' => 'string|min:6|confirmed',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->name     = $request->name;
  $data->email    = $request->email;
  $data->password = Hash::make($request->password);

  $data->save();

  return response()->json($data);

 }
 /**
  * Remove the specified resource from storage.
  *
  * @param  AppExpense  $expense
  * @return IlluminateHttpResponse
  */
 public function destroy(User $user)
 {
  $user->delete();
  return response()->json([
   "error"   => "user_destroyed",
   "message" => "El usuario se ha eliminado",
  ]);

 }

 /**
  * Create de process to authenticate a user.
  *
  *
  */
 public function authenticate(Request $request)
 {

  // Grab credentials from the request
  $credentials = $request->only('email', 'password');

  try {
   // Attempt to verify the credentials and create a token for the user
   if (!$token = JWTAuth::attempt($credentials)) {
    return response()->json([
     "error"   => "invalid_credentials",
     "message" => "Las credenciales ingresadas no son correctas.",
    ], 401);
   }
  } catch (JWTException $e) {
   // Something went wrong whilst attempting to encode the token
   return response()->json([
    "error"   => "could_not_create_token",
    "message" => "Servicio no disponbile por el momento.",
   ], 422);
  }

  // all good so return the token
  $user = User::where('email', $request->get('email'))->get();
  return response()->json([
   'user'  => $user,
   'token' => $token,
  ], 200);
 }

 /**
  * Create de process to register a user.
  */
 public function register(Request $request)
 {
  $validator = Validator::make($request->all(), [
   'name'     => 'required|string|max:255',
   'email'    => 'required|string|email|max:255|unique:users',
   'password' => 'required|string|min:6|confirmed',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $user = User::create([
   'name'     => $request->name,
   'email'    => $request->email,
   'password' => Hash::make($request->password),
  ]);

  $token = JWTAuth::fromUser($user);

  return response()->json(compact('user', 'token'), 200);
 }

}
