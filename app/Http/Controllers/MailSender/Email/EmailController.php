<?php

namespace App\Http\Controllers\MailSender\Email;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\MailSender\Email\Email;
use Illuminate\Http\Request;
use Mail;

class EmailController extends Controller
{
 /**
  * Send a contact Mail.
  *
  */
 public function contact(Request $request)
 {
  $type              = 'CONTACT';
  $contactAttributes = [
   'nameContact' => $request->name,
   'phone'       => $request->phone,
   'email'       => $request->email,
   'message'     => $request->message,
   'subject'     => '[corsosupplies.com] CONTACTO',
  ];

  $sendmail = Mail::to(env('MAIL_CONTACT'))->send(new SendMail($type, $contactAttributes, null));

  if (empty($sendmail)) {
   return response()->json(['message' => 'Correo enviado.'], 200);
  } else {
   return response()->json(['message' => 'Falla en envío de correo.'], 400);
  }

 }

 /**
  * Send a budget Mail.
  *
  */
 public function budget(Request $request)
 {
  $type             = 'BUDGET';
  $budgetAttributes = [
   'nameContact'   => $request->name,
   'phone'         => $request->phone,
   'email'         => $request->email,
   'country'       => $request->country,
   'state'         => $request->state,
   'city'          => $request->city,
   'pc'            => $request->pc,
   'product'       => $request->product,
   'specification' => $request->specification,
   'subject'       => '[corsosupplies.com] ¡COTIZA YA!',
  ];

  $sendmail = Mail::to(env('MAIL_CONTACT'))->send(new SendMail($type, null, $budgetAttributes));

  if (empty($sendmail)) {
   return response()->json(['message' => 'Correo enviado.'], 200);
  } else {
   return response()->json(['message' => 'Falla en envío de correo.'], 400);
  }
 }

}
