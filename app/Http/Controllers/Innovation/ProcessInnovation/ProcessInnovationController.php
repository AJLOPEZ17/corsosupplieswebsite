<?php

namespace App\Http\Controllers\Innovation\ProcessInnovation;

use App\Http\Controllers\Controller;
use App\Models\Innovation\ProcessInnovation\ProcessInnovation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProcessInnovationController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $processInovation = ProcessInnovation::first();
  return $processInovation;
 }
 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Innovation\ProcessInnovation\ProcessInnovation  $processInnovation
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $processInnovation)
 {
  $data = ProcessInnovation::find($processInnovation);

  $validator = Validator::make($request->all(), [
   'title_one'         => 'required|max:50',
   'description_one'   => 'required|max:255',
   'title_two'         => 'required|max:50',
   'description_two'   => 'required|max:255',
   'title_three'       => 'required|max:50',
   'description_three' => 'required|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->title_one         = $request->title_one;
  $data->description_one   = $request->description_one;
  $data->title_two         = $request->title_two;
  $data->description_two   = $request->description_two;
  $data->title_three       = $request->title_three;
  $data->description_three = $request->description_three;

  $data->save();

  return response()->json($data);
 }

}
