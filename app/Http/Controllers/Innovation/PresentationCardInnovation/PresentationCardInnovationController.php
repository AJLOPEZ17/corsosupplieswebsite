<?php

namespace App\Http\Controllers\Innovation\PresentationCardInnovation;

use App\Http\Controllers\Controller;
use App\Models\Innovation\PresentationCardInnovation\PresentationCardInnovation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PresentationCardInnovationController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $presentationCardInnovations = PresentationCardInnovation::first();
  return $presentationCardInnovations;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Inovation\PresentationCardInovation\PresentationCardInnovation  $presentationCardInnovation
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $presentationCardInnovation)
 {
  $data = PresentationCardInnovation::find($presentationCardInnovation);

  $validator = Validator::make($request->all(), [
   'title_one'       => 'required|max:50',
   'description_one' => 'required|max:255',
   'title_two'       => 'required|max:50',
   'description_two' => 'required|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->title_one       = $request->title_one;
  $data->description_one = $request->description_one;
  $data->title_two       = $request->title_two;
  $data->description_two = $request->description_two;

  $data->save();

  return response()->json($data);
 }

}
