<?php

namespace App\Http\Controllers\Products\Product;

use App\Http\Controllers\Controller;
use App\Models\Products\Product\Product;
use App\SupportFiles\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $products = Product::with('category')->get();
  return $products;
 }
 /**
  * Display a listing of a products of an especific category.
  *
  * @return \Illuminate\Http\Response
  */
 public function productOfCategory($category)
 {
  $productsOfCategory = Product::where('product_category_id', $category)->get();
  return $productsOfCategory;
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  $validator = Validator::make($request->all(), [
   'title'               => 'required|string|max:50',
   'description'         => 'required|max:255',
   'ingredients'         => 'required|max:255',
   'top'                 => 'required',
   'product_category_id' => 'required',
   'image'               => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $image = FileManager::uploadImage($request->image);
  }

  $product = Product::create([
   'title'               => $request->title,
   'description'         => $request->description,
   'ingredients'         => $request->ingredients,
   'top'                 => $request->top,
   'product_category_id' => $request->product_category_id,
   'image'               => $image->path,
   'image_name'          => $image->name,
  ]);

  return response()->json($product);
 }

 /**
  * Display the specified resource.
  *
  * @param  \App\Models\Products\Product\Product  $product
  * @return \Illuminate\Http\Response
  */
 public function show($product)
 {
  $data = Product::with('category')->find($product);
  return $data;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Products\Product\Product  $product
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $product)
 {
  $data = Product::find($product);

  $validator = Validator::make($request->all(), [
   'title'               => 'required|string|max:50',
   'description'         => 'required|max:255',
   'ingredients'         => 'required|max:255',
   'top'                 => 'required',
   'product_category_id' => 'required',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $validator = Validator::make($request->all(), [
    'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
   ]);
   $image = FileManager::uploadImage($request->image);
   Storage::disk('public')->delete($data->image);
   $data->image      = $image->path;
   $data->image_name = $image->name;
  }

  $data->title               = $request->title;
  $data->description         = $request->description;
  $data->ingredients         = $request->ingredients;
  $data->top                 = $request->top;
  $data->product_category_id = $request->product_category_id;

  $data->save();

  return response()->json($data);
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\Products\Product\Product  $product
  * @return \Illuminate\Http\Response
  */
 public function destroy(Product $product)
 {
  $imageToDelete = 'img/' . $product->image_name . '.jpg';
  Storage::disk('public')->delete($imageToDelete);
  $product->delete();
  return response()->json([
   "success" => "data_destroyed",
   "message" => "Registro eliminado.",
  ]);
 }

}
