<?php

namespace App\Http\Controllers\Products\ProductCategory;

use App\Http\Controllers\Controller;
use App\Models\Products\ProductCategory\ProductCategory;
use App\SupportFiles\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $productCategories = ProductCategory::paginate(1000);
  return $productCategories;
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  $validator = Validator::make($request->all(), [
   'name'  => 'required|string|max:255',
   'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $image = FileManager::uploadImage($request->image);
  }

  $productCategory = ProductCategory::create([
   'name'       => $request->name,

   'image'      => $image->path,
   'image_name' => $image->name,
  ]);

  return response()->json($productCategory);
 }

 /**
  * Display the specified resource.
  *
  * @param  \App\Models\Product\ProductCategory\ProductCategory  $productCategory
  * @return \Illuminate\Http\Response
  */
 public function show(ProductCategory $productCategory)
 {
  return $productCategory;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Product\ProductCategory\ProductCategory  $productCategory
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $productCategory)
 {
  $data = ProductCategory::find($productCategory);

  $validator = Validator::make($request->all(), [
   'name' => 'required|string|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $validator = Validator::make($request->all(), [
    'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
   ]);
   $image = FileManager::uploadImage($request->image);
   Storage::disk('public')->delete($data->image);
   $data->image      = $image->path;
   $data->image_name = $image->name;
  }

  $data->name = $request->name;
  $data->save();

  return response()->json($data);
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\Product\ProductCategory\ProductCategory  $productCategory
  * @return \Illuminate\Http\Response
  */
 public function destroy(ProductCategory $productCategory)
 {
  $imageToDelete = 'img/' . $productCategory->image_name . '.jpg';
  Storage::disk('public')->delete($imageToDelete);
  $productCategory->delete();
  return response()->json([
   "error"   => "data_destroyed",
   "message" => "Registro eliminado.",
  ]);
 }
}
