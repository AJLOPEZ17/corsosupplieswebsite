<?php

namespace App\Http\Controllers\General\Banner;

use App\Http\Controllers\Controller;
use App\Models\General\Banner\Banner;
use App\SupportFiles\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $banners = Banner::paginate(100);
  return $banners;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\General\Banner\Banner  $banner
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $banner)
 {
  $data = Banner::find($banner);

  $validator = Validator::make($request->all(), [
   'title' => 'required|string|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {

   $validator = Validator::make($request->all(), [
    'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
   ]);

   $image = FileManager::uploadImage($request->image);
   Storage::disk('public')->delete($data->image);
   $data->image      = $image->path;
   $data->image_name = $image->name;
  }

  $data->title = $request->title;

  $data->save();

  return response()->json($data);
 }

}
