<?php

namespace App\Http\Controllers\General\SocialMedia;

use App\Http\Controllers\Controller;
use App\Models\General\SocialMedia\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SocialMediaController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $socialMedia = SocialMedia::first();
  return $socialMedia;
 }
 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\General\SocialMedia\SocialMedia  $socialMedia
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $socialMedia)
 {

  $data      = SocialMedia::find($socialMedia);
  $validator = Validator::make($request->all(), [
   'longitude' => 'required|max:255',
   'latitude'  => 'required|max:255',
   'facebook'  => 'required|url',
   'twitter'   => 'required|url',
   'instagram' => 'required|url',
   'youtube'   => 'required|url',
   'whatsapp'  => 'required|url',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->longitude = $request->longitude;
  $data->latitude  = $request->latitude;
  $data->facebook  = $request->facebook;
  $data->twitter   = $request->twitter;
  $data->instagram = $request->instagram;
  $data->youtube   = $request->youtube;
  $data->whatsapp  = $request->whatsapp;

  $data->save();

  return response()->json($data);
 }

}
