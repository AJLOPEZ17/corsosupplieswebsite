<?php

namespace App\Http\Controllers\Home\SummaryCard;

use App\Http\Controllers\Controller;
use App\Models\Home\SummaryCard\SummaryCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SummaryCardController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $summaryCards = SummaryCard::paginate(3);
  return $summaryCards;
 }
 /**
  * Display the specified resource.
  *
  * @param  \App\Models\Home\SummaryCard\SummaryCard  $summaryCard
  * @return \App\Models\SummaryCard\SummaryCard
  */
 public function show(SummaryCard $summaryCard)
 {
  return $summaryCard;
 }
 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Home\SummaryCard\SummaryCard  $summaryCard
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $summaryCard)
 {
  $data = SummaryCard::find($summaryCard);

  $validator = Validator::make($request->all(), [
   'title'       => 'required|max:50',
   'description' => 'required|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->title       = $request->title;
  $data->description = $request->description;

  $data->save();

  return response()->json($data);

 }

}
