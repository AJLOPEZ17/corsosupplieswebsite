<?php

namespace App\Http\Controllers\Home\InformationBanner;

use App\Http\Controllers\Controller;
use App\Models\Home\InformationBanner\InformationBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InformationBannerController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index(Request $request)
 {
  $informationBanner = InformationBanner::first();
  return $informationBanner;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Home\InformationBanner\InformationBanner  $informationBanner
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $informationBanner)
 {

  $data = InformationBanner::find($informationBanner);

  $validator = Validator::make($request->all(), [
   'title'       => 'required|max:50',
   'description' => 'required|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->title       = $request->title;
  $data->description = $request->description;

  $data->save();

  return response()->json($data);

 }

}
