<?php

namespace App\Http\Controllers\Home\Service;

use App\Http\Controllers\Controller;
use App\Models\Home\Service\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $services = Service::paginate(2);
  return $services;
 }

 /**
  * Display the specified resource.
  *
  * @param  \App\Models\Home\Service\Service  $service
  * @return \Illuminate\Http\Response
  */
 public function show(Service $service)
 {
  return $service;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Home\Service\Service  $service
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $service)
 {
  $data = Service::find($service);

  $validator = Validator::make($request->all(), [
   'title'       => 'required|max:50',
   'description' => 'required|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  $data->title       = $request->title;
  $data->description = $request->description;

  $data->save();

  return response()->json($data);

 }

}
