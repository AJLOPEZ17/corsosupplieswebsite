<?php

namespace App\Http\Controllers\Home\CustomerImage;

use App\Http\Controllers\Controller;
use App\Models\Home\CustomerImage\CustomerImage;
use App\SupportFiles\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CustomerImageController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $customerImages = CustomerImage::paginate(100);
  return $customerImages;
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  $validator = Validator::make($request->all(), [
   'name'  => 'required|string|max:255',
   'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $image = FileManager::uploadImage($request->image);
  }

  $customerImage = CustomerImage::create([
   'name'       => $request->name,
   'image'      => $image->path,
   'image_name' => $image->name,
  ]);

  return response()->json($customerImage);
 }

 /**
  * Display the specified resource.
  *
  * @param  \App\Models\Home\CustomerImage\CustomerImage  $customerImage
  * @return \Illuminate\Http\Response
  */
 public function show(CustomerImage $customerImage)
 {
  return $customerImage;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Home\CustomerImage\CustomerImage  $customerImage
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $customerImage)
 {
  $data = CustomerImage::find($customerImage);

  if ($request->hasFile('image')) {
   $image = FileManager::uploadImage($request->image);
  }
  Storage::disk('public')->delete($data->image);

  $data->name       = $request->name;
  $data->image      = $image->path;
  $data->image_name = $image->name;

  $data->save();

  return response()->json($data);
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\Home\CustomerImage\CustomerImage  $customerImage
  * @return \Illuminate\Http\Response
  */
 public function destroy(CustomerImage $customerImage)
 {
  Storage::disk('public')->delete($customerImage->image);
  $customerImage->delete();
  return response()->json([
   "success" => "data_destroyed",
   "message" => "Registro eliminado.",
  ]);
 }
}
