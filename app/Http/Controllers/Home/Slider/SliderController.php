<?php

namespace App\Http\Controllers\Home\Slider;

use App\Http\Controllers\Controller;
use App\Models\Home\Slider\Slider;
use App\SupportFiles\FileManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $sliders = Slider::paginate(100);
  return $sliders;
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  //Request has URL
  if ($request->url != "") {
   $validator = Validator::make($request->all(), [
    'url' => 'required|url',
   ]);

   if ($validator->fails()) {
    return response()->json($validator->errors(), 400);
   }
   $slider = Slider::create([
    'url' => $request->url,
   ]);
   return response()->json($slider);
  }

  //Request has Image
  $validator = Validator::make($request->all(), [
   'title' => 'required|string|max:255',
   'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $image = FileManager::uploadImage($request->image);
  }

  $slider = Slider::create([
   'title'      => $request->title,
   'image'      => $image->path,
   'image_name' => $image->name,
  ]);

  return response()->json($slider);
 }

 /**
  * Display the specified resource.
  *
  * @param  \App\Models\Home\Slider\Slider  $slider
  * @return \Illuminate\Http\Response
  */
 public function show(Slider $slider)
 {
  return $slider;
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\Home\Slider\Slider  $slider
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $slider)
 {
  $data = Slider::find($slider);

//Request has URL
  if ($request->url != "") {
   $validator = Validator::make($request->all(), [
    'url' => 'required|url',
   ]);

   if ($validator->fails()) {
    return response()->json($validator->errors(), 400);
   }

   $data->url = $request->url;
   $data->save();

   return response()->json($data);
  }
//Request has Image
  $validator = Validator::make($request->all(), [
   'title' => 'required|string|max:255',
  ]);

  if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
  }

  if ($request->hasFile('image')) {
   $validator = Validator::make($request->all(), [
    'image' => 'required|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=300,min_height=600',
   ]);
   $image = FileManager::uploadImage($request->image);
   Storage::disk('public')->delete($data->image);
   $data->image      = $image->path;
   $data->image_name = $image->name;
  }

  $data->title = $request->title;

  $data->save();

  return response()->json($data);
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\Home\Slider\Slider  $slider
  * @return \Illuminate\Http\Response
  */
 public function destroy(Slider $slider)
 {
  Storage::disk('public')->delete($slider->image);
  $slider->delete();
  return response()->json([
   "success" => "data_destroyed",
   "message" => "Registro eliminado.",
  ]);
 }
}
