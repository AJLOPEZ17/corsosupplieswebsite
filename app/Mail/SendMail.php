<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
 use Queueable, SerializesModels;

 public $type;
 public $contactAttributes;
 public $budgetAttributes;
 /**
  * Create a new message instance.
  *
  * @return void
  */
 public function __construct($type, $contactAttributes, $budgetAttributes)
 {

  $this->type              = $type;
  $this->contactAttributes = $contactAttributes;
  $this->budgetAttributes  = $budgetAttributes;
 }

 /**
  * Build the message.
  *
  * @return $this
  */
 public function build()
 {
  if ($this->type == 'CONTACT') {
   return $this->view('mail-contact')
    ->from($this->contactAttributes['email'], $this->contactAttributes['nameContact'])
    ->subject($this->contactAttributes['subject']);
  } else if ($this->type == 'BUDGET') {
   return $this->view('mail-budget')
    ->from($this->budgetAttributes['email'], $this->budgetAttributes['nameContact'])
    ->subject($this->budgetAttributes['subject']);
  }
 }
}
