<?php

namespace App\Models\General\SocialMedia;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
 protected $filliable = [
  'longitude',
  'latitude',
  'facebook',
  'twitter',
  'instagram',
  'youtube',
  'whatsapp',
 ];
}
