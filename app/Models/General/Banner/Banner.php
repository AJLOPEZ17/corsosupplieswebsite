<?php

namespace App\Models\General\Banner;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
 protected $fillable = [
  'section',
  'title',
  'image',
  'image_name',
 ];
}
