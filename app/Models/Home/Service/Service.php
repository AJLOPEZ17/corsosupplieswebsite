<?php

namespace App\Models\Home\Service;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $filliable = [
        'title', 'description',
    ];
}
