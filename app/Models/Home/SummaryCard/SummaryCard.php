<?php

namespace App\Models\Home\SummaryCard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SummaryCard extends Model
{
 use SoftDeletes;

 protected $filliable = [
  'title', 'description',
 ];
}
