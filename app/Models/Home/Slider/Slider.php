<?php

namespace App\Models\Home\Slider;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
 use SoftDeletes;

 protected $fillable = [
  'url',
  'title',
  'image',
  'image_name',
 ];
}
