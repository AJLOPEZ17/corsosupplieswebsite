<?php

namespace App\Models\Home\InformationBanner;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformationBanner extends Model
{
 use SoftDeletes;

 protected $fillable = [
  'title',
  'description',
 ];
}
