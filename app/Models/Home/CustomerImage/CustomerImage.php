<?php

namespace App\Models\Home\CustomerImage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerImage extends Model
{
 use SoftDeletes;

 protected $fillable = [
  'name',
  'image',
  'image_name',
 ];
}
