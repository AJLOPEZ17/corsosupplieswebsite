<?php

namespace App\Models\Products\ProductCategory;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
 use SoftDeletes;

 protected $fillable = [
  'name',
  'image',
  'image_name',
 ];

 public function product()
 {
  return $this->hasMany(Product::class);
 }
}
