<?php

namespace App\Models\Products\Product;

use App\Models\Products\ProductCategory\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
 use SoftDeletes;

 protected $fillable = [
  'title',
  'description',
  'ingredients',
  'top',
  'product_category_id',
  'image',
  'image_name',
 ];

 public function category()
 {
  return $this->belongsTo(ProductCategory::class, 'product_category_id');
 }
}
