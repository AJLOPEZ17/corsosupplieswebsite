<?php

namespace App\Models\Innovation\PresentationCardInnovation;

use Illuminate\Database\Eloquent\Model;

class PresentationCardInnovation extends Model
{
 protected $filliable = [
  'title_one',
  'description_one',
  'title_two',
  'description_two',
 ];
}
