<?php

namespace App\Models\Innovation\ProcessInnovation;

use Illuminate\Database\Eloquent\Model;

class ProcessInnovation extends Model
{
 protected $filliable = [
  'title_one',
  'description_one',
  'title_two',
  'description_two',
  'title_three',
  'description_three',

 ];
}
