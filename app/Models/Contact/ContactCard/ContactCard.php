<?php

namespace App\Models\Contact\ContactCard;

use Illuminate\Database\Eloquent\Model;

class ContactCard extends Model
{
    protected $filliable = [
        'address',
        'phonen_umber',
        'email',
    ];
}
