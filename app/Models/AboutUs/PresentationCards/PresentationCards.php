<?php

namespace App\Models\AboutUs\PresentationCards;

use Illuminate\Database\Eloquent\Model;

class PresentationCards extends Model
{
    protected $filliable = [
        'title', 'description',
    ];
}
