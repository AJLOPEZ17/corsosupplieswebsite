<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});
 */
Route::group(['middleware' => ['cors']], function () {
//ADMIN ROUTES
 Route::group(['prefix' => 'auth'], function () {
  //Auth
  Route::post('/login', 'Auth\UserController@authenticate');
  Route::post('/register', 'Auth\UserController@register');
  //User
  Route::get('/user', 'Auth\UserController@index');
  Route::get('/user/{user}', 'Auth\UserController@show');
  Route::put('/user/{user}', 'Auth\UserController@update');
  Route::delete('/user/{user}', 'Auth\UserController@destroy');

  Route::group(['prefix' => 'home'], function () {
   //HOME
   //Information banner
   Route::get('/informationBanner', 'Home\InformationBanner\InformationBannerController@index');
   Route::put('/informationBanner/{informationbanner}', 'Home\InformationBanner\InformationBannerController@update');
   //Summary cards
   Route::get('/summaryCards', 'Home\SummaryCard\SummaryCardController@index');
   Route::get('/summaryCards/{summaryCard}', 'Home\SummaryCard\SummaryCardController@show');
   Route::put('/summaryCards/{summaryCard}', 'Home\SummaryCard\SummaryCardController@update');
   //Services
   Route::get('/services', 'Home\Service\ServiceController@index');
   Route::get('/services/{service}', 'Home\Service\ServiceController@show');
   Route::put('/services/{service}', 'Home\Service\ServiceController@update');
   //Customer images
   Route::post('/customerImage', 'Home\CustomerImage\CustomerImageController@store');
   Route::get('/customerImage', 'Home\CustomerImage\CustomerImageController@index');
   Route::get('/customerImage/{customerImage}', 'Home\CustomerImage\CustomerImageController@show');
   Route::put('/customerImage/{customerImage}', 'Home\CustomerImage\CustomerImageController@update');
   Route::delete('/customerImage/{customerImage}', 'Home\CustomerImage\CustomerImageController@destroy');
   //Slider
   Route::post('/slider', 'Home\Slider\SliderController@store');
   Route::get('/slider', 'Home\Slider\SliderController@index');
   Route::get('/slider/{slider}', 'Home\Slider\SliderController@show');
   Route::put('/slider/{slider}', 'Home\Slider\SliderController@update');
   Route::delete('/slider/{slider}', 'Home\Slider\SliderController@destroy');
  });
  Route::group(['prefix' => 'aboutUs'], function () {
   //ABOUT US
   //Presentation cards
   Route::get('/presentationCards', 'AboutUs\PresentationCards\PresentationCardsController@index');
   Route::put('/presentationCards/{presentationCard}', 'AboutUs\PresentationCards\PresentationCardsController@update');
  });
  Route::group(['prefix' => 'general'], function () {
   //GENERAL
   //Social media
   Route::get('/socialMedia', 'General\SocialMedia\SocialMediaController@index');
   Route::put('/socialMedia/{socialMedia}', 'General\SocialMedia\SocialMediaController@update');
   //Banners
   Route::get('/banner', 'General\Banner\BannerController@index');
   Route::put('/banner/{banner}', 'General\Banner\BannerController@update');
  });
  Route::group(['prefix' => 'contact'], function () {
   //CONTACT
   //Contact card
   Route::get('/contactCard', 'Contact\ContactCard\ContactCardController@index');
   Route::put('/contactCard/{contactCard}', 'Contact\ContactCard\ContactCardController@update');
  });
  Route::group(['prefix' => 'product'], function () {
   //PRODUCTS
   //Product categories
   Route::get('/productCategory', 'Products\ProductCategory\ProductCategoryController@index');
   Route::post('/productCategory', 'Products\ProductCategory\ProductCategoryController@store');
   Route::get('/productCategory/{productCategory}', 'Products\ProductCategory\ProductCategoryController@show');
   Route::put('/productCategory/{productCategory}', 'Products\ProductCategory\ProductCategoryController@update');
   Route::delete('/productCategory/{productCategory}', 'Products\ProductCategory\ProductCategoryController@destroy');
   //Product
   Route::post('/', 'Products\Product\ProductController@store');
   Route::get('/', 'Products\Product\ProductController@index');
   Route::get('/{product}', 'Products\Product\ProductController@show');
   Route::put('/{product}', 'Products\Product\ProductController@update');
   Route::delete('/delete/{product}', 'Products\Product\ProductController@destroy');
   //Product client
   Route::get('/category/{category}', 'Products\Product\ProductController@productOfCategory');

  });
  Route::group(['prefix' => 'innovation'], function () {
   //INNOVATION
   //Presentation cards
   Route::get('/presentationCardsInnovation', 'Innovation\PresentationCardInnovation\PresentationCardInnovationController@index');
   Route::put('/presentationCardsInnovation/{presentationCardInnovation}', 'Innovation\PresentationCardInnovation\PresentationCardInnovationController@update');

   //Process innovation
   Route::get('/processInnovation', 'Innovation\ProcessInnovation\ProcessInnovationController@index');
   Route::put('/processInnovation/{processInnovation}', 'Innovation\ProcessInnovation\ProcessInnovationController@update');
  });
 });
 Route::group(['prefix' => 'contact'], function () {
  //CONTACT CLIENT
  //Mail
  Route::post('/mail', 'MailSender\Email\EmailController@contact');
 });
 Route::group(['prefix' => 'budget'], function () {
  //BUDGET CLIENT
  //Mail
  Route::post('/mail', 'MailSender\Email\EmailController@budget');
 });

});
