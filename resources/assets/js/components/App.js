import React, { Component } from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

//Components
import Header from "./components/Header";
import Example from "./Example";
import AdminLayout from "./layouts/Admin/Admin.js";
import RTLLayout from "./layouts/RTL/RTL.js";

//Styles
import "./assets/scss/black-dashboard-react.scss";
import "./assets/demo/demo.css";
import "./assets/css/nucleo-icons.css";

const hist = createBrowserHistory();

class App extends Component {
    render() {
        return (
            <Router history={hist}>
                <Switch>
                    <Route
                        path="/admin"
                        render={props => <AdminLayout {...props} />}
                    />
                    <Route
                        path="/rtl"
                        render={props => <RTLLayout {...props} />}
                    />
                    <Redirect from="/" to="/admin/dashboard" />
                </Switch>
            </Router>
        );
    }
}

ReactDOM.render(
    <Router history={hist}>
        <div>
            {/* <Header /> */}
            <Switch>
                {/* <Route exact path="/" component={Example} /> */}
                <Route
                    path="/admin"
                    render={props => <AdminLayout {...props} />}
                />
                <Route path="/rtl" render={props => <RTLLayout {...props} />} />
                <Redirect from="/" to="/admin/dashboard" />
            </Switch>
        </div>
    </Router>,
    document.getElementById("app")
);
