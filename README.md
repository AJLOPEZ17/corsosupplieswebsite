## AL CREAR O CLONAR UN PROYECTO

cp .env.example .env
php artisan key:generate

###### ** Crear BD en el manejador **

###### ** Configurar .env con los datos del manejador en la parte de DB_CONNECTION **

php artisan migrate --seed

## USUARIOS PARA LOGIN

USER: superadmin@admin.com
PASSWD: superadmin

USER: admin@admin.com
PASSWD: admin

## BACKEND

## CREAR RECURSOS

## MODEL Y MIGRATION

php artisan make:model Models/Modulo/NombreCarpeta/NombreArchivo -m

## CONTROLLER

php artisan make:controller NombreCarpeta/NombreArchivo -r --model App\Models\NombreCarpeta\NombreArchivo

## FACTORY

php artisan make:factory NombreArchivo --model App\Models\NombreCarpeta\NombreArchivo

## SEEDER

php artisan make:seeder NombreArchivoTableSeeder
NOTA: Importar el modelo una vez que se crea el archivo (manualmente)

## UNIT TEST

php artisan make:test NombreArchivoTest

EJECUCIÓN DE TODAS PRUEBAS UNITARIAS: .\vendor\bin\phpunit
EJECUCIÓN DE UNA PRUEBA UNITARIA: .\vendor\bin\phpunit --filter NombreArchivoTest

### Configurar archivos phpunit.xml y database.php

## LIMPIAR CACHÉ

composer dump-autoload
