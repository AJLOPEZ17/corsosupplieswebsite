<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Home\Service\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
 return [
  'title'       => $faker->words(3, true),
  'description' => $faker->text,
 ];
});
