<?php

use App\Models\Home\SummaryCard\SummaryCard;
use Faker\Generator as Faker;

$factory->define(SummaryCard::class, function (Faker $faker) {
 return [
  'title'       => $faker->words(3, true),
  'description' => $faker->text,
 ];
});
