<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products\Product\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
 return [
  'title'               => $faker->words(3, true),
  'description'         => $faker->text(),
  'ingredients'         => $faker->text(),
  'top'                 => $faker->numberBetween(0, 1),
  'product_category_id' => $faker->numberBetween(1, 4),
  'image'               => 'https://placeholder.com/300x600',
  'image_name'          => $faker->words(3, true),
 ];
});
