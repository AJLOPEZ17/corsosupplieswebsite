<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contact\ContactCard\ContactCard;
use Faker\Generator as Faker;

$factory->define(ContactCard::class, function (Faker $faker) {
 return [
  'address'      => $faker->address,
  'phone_number' => $faker->randomNumber(),
  'email'        => $faker->email,
 ];
});
