<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\General\SocialMedia\SocialMedia;
use Faker\Generator as Faker;

$factory->define(SocialMedia::class, function (Faker $faker) {
 return [
  'longitude' => $faker->longitude(),
  'latitude'  => $faker->latitude(),
  'facebook'  => $faker->url,
  'twitter'   => $faker->url,
  'instagram' => $faker->url,
  'youtube'   => $faker->url,
  'whatsapp'  => $faker->url,
 ];
});
