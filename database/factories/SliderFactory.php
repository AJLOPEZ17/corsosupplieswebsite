<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Home\Slider\Slider;
use Faker\Generator as Faker;

$factory->define(Slider::class, function (Faker $faker) {
 return [
  'url'        => $faker->url,
  'title'      => $faker->words(3, true),
  'image'      => 'https://placeholder.com/300x600',
  'image_name' => $faker->words(3, true),
 ];
});
