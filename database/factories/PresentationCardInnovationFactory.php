<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Innovation\PresentationCardInnovation\PresentationCardInnovation;
use Faker\Generator as Faker;

$factory->define(PresentationCardInnovation::class, function (Faker $faker) {
 return [
  'title_one'       => $faker->words(3, true),
  'description_one' => $faker->text,
  'title_two'       => $faker->words(3, true),
  'description_two' => $faker->text,
 ];
});
