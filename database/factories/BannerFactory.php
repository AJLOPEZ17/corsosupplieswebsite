<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\General\Banner\Banner;
use Faker\Generator as Faker;

$factory->define(Banner::class, function (Faker $faker) {
 return [
  'section'    => $faker->words(3, true),
  'title'      => $faker->words(3, true),
  'image'      => 'https://placeholder.com/300x600',
  'image_name' => $faker->words(3, true),
 ];
});
