<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Innovation\ProcessInnovation\ProcessInnovation;
use Faker\Generator as Faker;

$factory->define(ProcessInnovation::class, function (Faker $faker) {
 return [
  'title_one'         => $faker->words(3, true),
  'description_one'   => $faker->text,
  'title_two'         => $faker->words(3, true),
  'description_two'   => $faker->text,
  'title_three'       => $faker->words(3, true),
  'description_three' => $faker->text,
 ];
});
