<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AboutUs\PresentationCards\PresentationCards;
use Faker\Generator as Faker;

$factory->define(PresentationCards::class, function (Faker $faker) {
 return [
  'title'       => $faker->words(3, true),
  'description' => $faker->text,
 ];
});
