<?php

use App\Models\Home\CustomerImage\CustomerImage;
use Faker\Generator as Faker;

$factory->define(CustomerImage::class, function (Faker $faker) {
 return [
  'name'       => $faker->words(3, true),
  'image'      => 'https://placeholder.com/300x600',
  'image_name' => $faker->words(3, true),
 ];
});
