<?php

use App\Models\Home\InformationBanner\InformationBanner;
use Faker\Generator as Faker;

$factory->define(InformationBanner::class, function (Faker $faker) {
 return [
  'title'       => $faker->words(3, true),
  'description' => $faker->text(),
 ];
});
