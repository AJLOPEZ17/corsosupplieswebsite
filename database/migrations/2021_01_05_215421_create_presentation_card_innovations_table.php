<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresentationCardInnovationsTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('presentation_card_innovations', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->string('title_one');
   $table->text('description_one');
   $table->string('title_two');
   $table->text('description_two');
   $table->timestamps();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::dropIfExists('presentation_card_innovations');
 }
}
