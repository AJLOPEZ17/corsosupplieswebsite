<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialMediaTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('social_media', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->string('longitude');
   $table->string('latitude');
   $table->string('facebook');
   $table->string('twitter');
   $table->string('instagram');
   $table->string('youtube');
   $table->string('whatsapp');
   $table->timestamps();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::dropIfExists('social_media');
 }
}
