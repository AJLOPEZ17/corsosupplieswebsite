<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessInnovationsTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('process_innovations', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->string('title_one');
   $table->text('description_one');
   $table->string('title_two');
   $table->text('description_two');
   $table->string('title_three');
   $table->text('description_three');
   $table->timestamps();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::dropIfExists('process_innovations');
 }
}
