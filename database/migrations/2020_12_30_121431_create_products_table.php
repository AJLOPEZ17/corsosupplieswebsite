<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
 /**
  * Run the migrations.
  *
  * @return void
  */
 public function up()
 {
  Schema::create('products', function (Blueprint $table) {
   $table->bigIncrements('id');
   $table->string('title');
   $table->text('description');
   $table->text('ingredients');
   $table->boolean('top');
   $table->bigInteger('product_category_id')->unsigned();
   $table->string('image');
   $table->string('image_name');
   $table->timestamps();
   $table->softDeletes();
  });
 }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
 public function down()
 {
  Schema::dropIfExists('products');
 }
}
