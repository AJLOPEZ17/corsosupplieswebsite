<?php

use App\Models\AboutUs\PresentationCards\PresentationCards;
use Illuminate\Database\Seeder;

class PresentationCardsTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(PresentationCards::class, 3)->create();
 }
}
