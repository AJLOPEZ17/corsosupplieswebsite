<?php

use App\Models\General\Banner\Banner;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run(Faker $faker)
 {
  Banner::create([
   'section'    => 'NOSOTROS',
   'title'      => $faker->words(3, true),
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => $faker->words(3, true),
  ]);
  Banner::create([
   'section'    => 'PRODUCTOS',
   'title'      => $faker->words(3, true),
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => $faker->words(3, true),
  ]);
  Banner::create([
   'section'    => 'CONTACTO',
   'title'      => $faker->words(3, true),
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => $faker->words(3, true),
  ]);
  Banner::create([
   'section'    => 'INNOVACION',
   'title'      => $faker->words(3, true),
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => $faker->words(3, true),
  ]);
  Banner::create([
   'section'    => 'COTIZA',
   'title'      => $faker->words(3, true),
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => $faker->words(3, true),
  ]);
 }
}
