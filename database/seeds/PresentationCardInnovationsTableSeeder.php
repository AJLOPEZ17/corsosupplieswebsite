<?php

use App\Models\Innovation\PresentationCardInnovation\PresentationCardInnovation;
use Illuminate\Database\Seeder;

class PresentationCardInnovationsTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(PresentationCardInnovation::class)->create();
 }
}
