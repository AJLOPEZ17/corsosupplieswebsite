<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
 /**
  * Seed the application's database.
  *
  * @return void
  */
 public function run()
 {
  $this->call(UsersTableSeeder::class);
  $this->call(InformationBannerTableSeeder::class);
  $this->call(SummaryCardsTableSeeder::class);
  $this->call(ServicesTableSeeder::class);
  $this->call(PresentationCardsTableSeeder::class);
  $this->call(SocialMediaTableSeeder::class);
  $this->call(ContactCardsTableSeeder::class);
  $this->call(ProductCategoriesTableSeeder::class);
  $this->call(CustomerImagesTableSeeder::class);
  $this->call(BannersTableSeeder::class);
  $this->call(SlidersTableSeeder::class);
  $this->call(ProductsTableSeeder::class);
  $this->call(PresentationCardInnovationsTableSeeder::class);
  $this->call(ProcessInnovationsTableSeeder::class);
 }
}
