<?php

use App\Models\Home\Service\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(Service::class, 2)->create();
 }
}
