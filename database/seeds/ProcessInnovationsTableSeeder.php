<?php

use App\Models\Innovation\ProcessInnovation\ProcessInnovation;
use Illuminate\Database\Seeder;

class ProcessInnovationsTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(ProcessInnovation::class)->create();
 }
}
