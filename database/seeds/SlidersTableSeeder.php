<?php

use App\Models\Home\Slider\Slider;
use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(Slider::class, 5)->create();
 }
}
