<?php

use App\Models\Home\InformationBanner\InformationBanner;
use Illuminate\Database\Seeder;

class InformationBannerTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(InformationBanner::class, 1)->create();
 }
}
