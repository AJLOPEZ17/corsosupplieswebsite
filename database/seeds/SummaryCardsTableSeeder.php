<?php

use App\Models\Home\SummaryCard\SummaryCard;
use Illuminate\Database\Seeder;

class SummaryCardsTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(SummaryCard::class, 3)->create();
 }
}
