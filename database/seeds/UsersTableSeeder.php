<?php

use App\Models\User\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
 public function run()
 {
  User::create([
   'name'     => 'Superadmin',
   'email'    => 'superadmin@admin.com',
   'password' => Hash::make('superadmin'),
  ]);
  User::create([
   'name'     => 'Admin',
   'email'    => 'admin@admin.com',
   'password' => Hash::make('admin'),
  ]);
 }
}
