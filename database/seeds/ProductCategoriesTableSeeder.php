<?php

use App\Models\Products\ProductCategory\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  ProductCategory::create([
   'name'       => 'Galleta',
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => 'Galleta',
  ]);
  ProductCategory::create([
   'name'       => 'Abarrote',
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => 'Abarrote',
  ]);
  ProductCategory::create([
   'name'       => 'Trigo inflado',
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => 'Trigo inflado',
  ]);
  ProductCategory::create([
   'name'       => 'Barra de cereal',
   'image'      => 'https://placeholder.com/300x600',
   'image_name' => 'Barra de cereal',
  ]);
 }
}
