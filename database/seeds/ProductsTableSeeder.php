<?php

use App\Models\Products\Product\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(Product::class, 20)->create();
 }
}
