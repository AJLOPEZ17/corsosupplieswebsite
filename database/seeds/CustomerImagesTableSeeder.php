<?php

use App\Models\Home\CustomerImage\CustomerImage;
use Illuminate\Database\Seeder;

class CustomerImagesTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(CustomerImage::class, 3)->create();
 }
}
