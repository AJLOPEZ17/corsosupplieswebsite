<?php

use App\Models\Contact\ContactCard\ContactCard;
use Illuminate\Database\Seeder;

class ContactCardsTableSeeder extends Seeder
{
 /**
  * Run the database seeds.
  *
  * @return void
  */
 public function run()
 {
  factory(ContactCard::class)->create();
 }
}
